<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Pete Valeo | Web Designer, Developer, Dabbler, Didact in Madison, Wisconsin</title>
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet">
    <link rel="stylesheet" href="style/main.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://use.typekit.net/bix2dfh.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>
</head>
<body>
<div id="Container" class="container">
<header>
<div id="branding">
<div id="identification">
                <h1>Pete Valeo</h1>
                <h2>Web Designer, Developer, Dabbler, Didact</h2>
            </div>
<div id="logo">
       <?php readfile('assets/P.svg'); 
        ?> 
        </div>
       
</header>
<section id="mainContent">
<h2>I would love to help you make a home on the internet.</h2>


<h2>What I Can Do For You</h2>
<section id="Services">
    <section class="service">
       <h3>Web Design</h3>
    <div class="description">
        <p>Need a new website? I can do that.</p>

        <p>Whether it's a custom site for your small business, personal landing page, blog or portfolio, I'll help you get online with a site you love.</p>
    </div>    
    <img src="assets/Dev.png" alt="Vector art of HTML code">    
        
    </section>
    
    <section class="service">           
<h3>Development </h3>
    <img src="assets/Dev.png" alt="Vector art of HTML code">    
<div class="description">
               <p>Already have a site designed and need someone to build it? I can do that too. </p>
                <p>I like WordPress, but I am comfortable with most of the usual suspects when it comes to web development. Let's talk.</p>
</div>
    </section>
    
    <section class="service"><h3>Technology Training</h3>
<div class="description"><p>Wish you knew more about how things work online, how to retouch images, or how to edit a podcast? I offer customized training on WordPress, Photoshop, Illustrator, InDesign, Audition, and more.</p>
            </div>
    <img src="assets/Lightbulb.png" alt="Vector art of HTML code">    
    </section>
</section>

</section>

<section id="additionalContent">   
<h2>Madison, WI is where I call home, but wherever you find yourself, I can help you settle your own plot of internet land. <h2>

<div id="contact">
    <a  href="#contact">Let's work.</a>
    <form id="form" action="send.php" method="POST">
        <div>
            <p>Name:</p>
            <input type="textarea" name="name" rows="2" placeholder="Jamie Smith"><br/>
            <p>Email:</p>
            <input type="textarea" name="email" rows="2"placeholder="jamie@email.com"><br/>
            <p>Message:</p>
            <textarea name="message" id="" cols="30" rows="6"></textarea>
            <button type="submit">Send</button>
        </div>
    </form>
    <a href="#close">X</a>
</div>
</section>
<div id="CloudContainer">   <?php readfile('assets/Clouds.svg'); ?></div>
<footer id="#close" class="bottom">
   <?php readfile('assets/Skyline.svg'); 

    
    ?>
</footer>

</div>
</body>

<!--
<script type="text/javascript" src="main.js">    
</script>
-->

<script type="text/javascript">
    <?php  readfile('main.js'); ?>
</script>
</html>