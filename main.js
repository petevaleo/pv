Easing = {
  // no easing, no acceleration
  linear: function (t) { return t },
  // accelerating from zero velocity
  easeInQuad: function (t) { return t*t },
  // decelerating to zero velocity
  easeOutQuad: function (t) { return t*(2-t) },
  // acceleration until halfway, then deceleration
  easeInOutQuad: function (t) { return t<.5 ? 2*t*t : -1+(4-2*t)*t },
  // accelerating from zero velocity 
  easeInCubic: function (t) { return t*t*t },
  // decelerating to zero velocity 
  easeOutCubic: function (t) { return (--t)*t*t+1 },
  // acceleration until halfway, then deceleration 
  easeInOutCubic: function (t) { return t<.5 ? 4*t*t*t : (t-1)*(2*t-2)*(2*t-2)+1 },
  // accelerating from zero velocity 
  easeInQuart: function (t) { return t*t*t*t },
  // decelerating to zero velocity 
  easeOutQuart: function (t) { return 1-(--t)*t*t*t },
  // acceleration until halfway, then deceleration
  easeInOutQuart: function (t) { return t<.5 ? 8*t*t*t*t : 1-8*(--t)*t*t*t },
  // accelerating from zero velocity
  easeInQuint: function (t) { return t*t*t*t*t },
  // decelerating to zero velocity
  easeOutQuint: function (t) { return 1+(--t)*t*t*t*t },
  // acceleration until halfway, then deceleration 
  easeInOutQuint: function (t) { return t<.5 ? 16*t*t*t*t*t : 1+16*(--t)*t*t*t*t }
}

function hsl(a,b,c,d) {
    if(d){
        return "hsla("+a+", "+b+"%, "+c+"%, "+d+")";
    }
    else {
        return "hsl("+a+", "+b+"%, "+c+"%)";
    }
}


function scrollWords() {
    var branding = document.getElementById("identification");
    var words =  branding.children[1];
    var text = words.innerHTML.split(',');
    text.unshift(text[0].split(' ')[0]);
    text[1] = text[1].split(' ')[1];
//console.log(text);
    var web = document.createElement('span');
    var jobs = document.createElement('span');
    web.innerHTML = text[0]+" ";
//console.log(web);
    jobs.innerHTML = text[1]+text[2]+text[3]+text[4]+" "+text[1];
//console.log(words);
    words.innerHTML = '';
    words.appendChild(web).appendChild(jobs);
//    words.appendChild(jobs);
//console.log(jobs);
};


function stars() {
    var cont = document.querySelector('html');
    
    if (canvMake = document.getElementById('Canvas')) {
//        var canvMake = document.getElementById('Canvas');        
        canvMake.width = cont.scrollWidth;
        canvMake.height = cont.scrollHeight;
    }
    
    if (!document.getElementById('Canvas')) {
        var canvMake = document.createElement('canvas');
        
        canvMake.id = "Canvas";
        canvMake.width = cont.scrollWidth;
        canvMake.height = cont.scrollHeight;
//console.log(canvMake);
        cont.appendChild(canvMake);
    }

    var ctx = canvMake.getContext('2d');
//    ctx.fillStyle = '#77f';
    ctx.fillStyle = '#CCF';
    
    ctx.clearRect(0,0,canvMake.width, canvMake.height);    
    
    for (var i = 0; i < canvMake.width; i++) {
        x = Math.round(Math.random() * canvMake.width);
        y = Math.round(Math.random() * canvMake.height);
        s = rand(2);
        ctx.fillRect(x, y, s, s);
    }
//    ctx.clearRect(0,0,canvMake.width/2, canvMake.height/2);    
    
    ctx.fillStyle = "#FFF";
    
//    requestAnimationFrame(setInterval(stars, 300));
}
    
function rand(arg) {
    return Math.round(Math.random()*arg);
}    
    


    scrollWords();
    stars();
window.addEventListener('resize',function(){
//                stars();
    });    
    
    
var skyColor = document.getElementById('Skyline');
var logo = document.getElementById('pBody');
var iR = document.getElementById('Inner_Ring');
var oR = document.getElementById('Outer_Ring');
var stem = document.getElementById('StemP');

var body = document.querySelector('body');
var header = document.querySelector('header');
//var logoCont = document.getElementById('logo');
var logoCont = document.getElementById('letterP');
var identification = document.getElementById("branding");


skyColor.children[0].style.fill = hsl(215,80,16,.99);

    idStyle = window.getComputedStyle(identification);
    
    var padLeft = idStyle.getPropertyValue('padding-left');
    var pLeft = padLeft;
    var padTop = idStyle.getPropertyValue('padding-top');
    var pTop = padTop;

function valBetween(v, min, max) {
    return (Math.min(max, Math.max(min, v)));
}

var scrollAmt = function() {
//    var sH = document.body.scrollHeight;
//    var sT = document.body.scrolTop;
//    var cH = document.body.clientHeight;
//    var iH = window.innerHeight;
//    var pYO = window.pageYOffset;
//    console.log("scrollHeight: "+document.body.scrollHeight);
//    console.log("scrollTop: "+document.body.scrollTop);
//    console.log("clientHeight: "+document.body.clientHeight);
//    console.log("innerHeight: "+window.innerHeight);
//    console.log("pageYOffset: "+window.pageYOffset);
//    console.log(pageYOffset/(cH-iH));
//    return pYO/(cH-iH);
    return Math.max((window.pageYOffset/(document.body.clientHeight-window.innerHeight))*100);
}

var getStyleValue = function(el,attr) {
    var index = window.getComputedStyle(el);
    var ind;
    for (item in index) {
        if (index[item] == attr) {
            ind = index[item];
        }
    }
    return index.getPropertyValue(ind);
}

var logoOrigHeight = getStyleValue(logoCont,'height');

var linGrad = function(a,b,c,d) {
    return "linear-gradient(0deg, "+a+" "+b+"%, "+c+" "+d+"%)";
}  

var dePixel = function(arg) {
    return Number(arg.slice(0,arg.length-2));
}    
    var canv = document.getElementById("Canvas");
    canv.style.opacity = 1.01-(scrollAmt()/100);
    
    var lightness = valBetween(scrollAmt(),16,60);
    var gradAmt = valBetween(scrollAmt(),15,46);

//body.style.backgroundImage = linGrad(hsl(215,80,valBetween(scrollAmt(),15,60)),100,hsl(215,80,1.5*valBetween(scrollAmt(),15,46)),100);    

//var bodPadTop = dePixel(getStyleValue(body,'padding-top'));

window.addEventListener('scroll', function() {
//    skyColor.children[0].style.fill = hsl(window.pageYOffset/15, 50,20,.95);  

    var scroll = scrollAmt();
    
    var percent = (1-(scrollAmt()/100));
    
    idStyle = window.getComputedStyle(identification);
    var padLeft = idStyle.getPropertyValue('padding-left');
    var padTop = idStyle.getPropertyValue('padding-top');    
    var p = dePixel(pTop)*(100-scrollAmt())/100+"px";    
    identification.style.paddingTop = p;
    
    if (window.pageYOffset <= 200 ) {
        
        identification.style.backgroundColor = hsl(215,80,15, .01+window.pageYOffset/200);
        
        stem.style.opacity = (200-(window.pageYOffset))/100;
        iR.style.opacity = (200-(window.pageYOffset))/100;
        logoCont.style.transform = "rotateZ("+(window.pageYOffset/200*22)+"deg)";   

}
    
    if (window.pageYOffset > 200 ) {
        stem.style.opacity = 0;
        iR.style.opacity = 0;
        canv.style.opacity = .75*percent;
//        header.innerHTML = 1.01-scrollAmt
        logoCont.style.transform = "rotateZ(22deg)";
        identification.style.backgroundColor = hsl(215,80,15, 1);
    }
});

var send = function() {
//    alert("Send!");
    window.location="#close";
var ajax = new XMLHttpRequest();
console.log(ajax);
    ajax.open("POST","send.php",false);
    ajax.send();
    console.log(ajax.responseText);
}

//var ajax = XMLHttpRequest();
//console.log(ajax);
